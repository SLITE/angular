import {
    Component,
    View,
    provide
} from "angular2/core";

 
import {bootstrap} from "angular2/platform/browser"; 

import {
    RouteConfig,
    RouterLink,
    RouterOutlet,
    Route,
    ROUTER_DIRECTIVES,
    ROUTER_PROVIDERS,
    Location,
    LocationStrategy,
    PathLocationStrategy,
    Router
} from 'angular2/router';

import {HTTP_PROVIDERS} from 'angular2/http';

import { RouteConfig, ROUTER_DIRECTIVES } from 'angular2/router-deprecated';
 
import { Default } from './default/default';
import { Page2 } from './page2/page2';
 
@Component({
    selector: "my-app",
    templateUrl: "./app/app.html",
    directives: [ROUTER_DIRECTIVES]
})
 
@RouteConfig([
    { path: "/", name: "Default", component: Default },
    { path: "/page2", name: "Page2", component: Page2 }
])
 
class App {
    router: Router;
    location: Location;
 
    constructor(router: Router, location: Location) {
        this.router = router;
        this.location = location;
    }
 
}
 
bootstrap(App, [ROUTER_PROVIDERS, HTTP_PROVIDERS, provide(LocationStrategy, {useClass: PathLocationStrategy})]);