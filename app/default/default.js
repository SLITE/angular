System.register(["angular2/core", "../header/head", "../services/dataProvider"], function(exports_1) {
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, head_1, dataProvider_1;
    var Default;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (head_1_1) {
                head_1 = head_1_1;
            },
            function (dataProvider_1_1) {
                dataProvider_1 = dataProvider_1_1;
            }],
        execute: function() {
            Default = (function () {
                function Default(provider) {
                    this.provider = provider;
                    this.users = [{ "name": "Ala", "age": 22 }, { "name": "Kim", "age": 18 }];
                    this.students = [{ "name": "Greg", "age": 10 }, { "name": "Gon", "age": 18 }];
                    this.workers = [{ "name": "Alex", "age": 14 }, { "name": "Tim", "age": 12 }];
                    this.itemsOption = [{ "title": "itm1" }, { "title": "itm2" }];
                    this.showSpan = true;
                }
                Default.prototype.getDataA = function () {
                    var _this = this;
                    /*this.provider.getUsrs('http://localhost:3000/users/data');
                    users = this.provider.users;*/
                    this.provider.getUsrs('http://localhost:3000/users/data')
                        .subscribe(function (users) { return _this.setVal(users.json()); });
                };
                Default.prototype.setVal = function (val) {
                    console.log("wrk");
                    this.users = val;
                };
                Default.prototype.setlist = function (lv) {
                    if (lv === 'add') {
                        for (var i = 0; i < 3000; i++) {
                            this.itemsOption[i] = { "title": "item" + i * 2 };
                        }
                    }
                    else {
                        this.itemsOption = [];
                    }
                };
                Default.prototype.upAge = function () {
                    for (var i = 0; i < this.users.length; i++) {
                        this.users[i].age = ++this.users[i].age;
                    }
                };
                Default.prototype.changeSpan = function () {
                    this.itemsOption[10].title = Math.random();
                    if (this.showSpan) {
                    }
                    else {
                    }
                };
                Default.prototype.setData = function (type) {
                    if (type === 'worker') {
                        this.users = this.workers;
                    }
                    else {
                        this.users = this.students;
                    }
                };
                Default = __decorate([
                    core_1.Component({
                        selector: 'default',
                        templateUrl: "app/default/default.html",
                        directives: [head_1.Head],
                        providers: [dataProvider_1.DataProvider]
                    }), 
                    __metadata('design:paramtypes', [dataProvider_1.DataProvider])
                ], Default);
                return Default;
            })();
            exports_1("Default", Default);
        }
    }
});
//# sourceMappingURL=default.js.map