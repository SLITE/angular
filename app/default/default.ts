import {Component} from "angular2/core";
import {Head} from "../header/head";
import {DataProvider} from "../services/dataProvider";

 
@Component({
    selector: 'default',
    templateUrl: "app/default/default.html",
    directives: [Head]
    providers: [DataProvider]
})

export class Default {
	public users = [{"name":"Ala","age":22},{"name":"Kim","age":18}];
	public students = [{"name":"Greg","age":10},{"name":"Gon","age":18}];
	public workers = [{"name":"Alex","age":14},{"name":"Tim","age":12}];
	public itemsOption = [{"title":"itm1"},{"title":"itm2"}];
	public showSpan = true;

    constructor(private provider: DataProvider) { }

    getDataA()
    {
        /*this.provider.getUsrs('http://localhost:3000/users/data');
    	users = this.provider.users;*/
        this.provider.getUsrs('http://localhost:3000/users/data')
           .subscribe(users => this.setVal(users.json()));
    }

    setVal(val){
        console.log("wrk");
        this.users = val;
    }

    setlist(lv){
    	if(lv==='add')
    	{
    		for(var i=0;i<3000;i++){
 			this.itemsOption[i] = {"title":"item"+i*2};
    		}
    	}
    	else
    	{
    		this.itemsOption=[];
    	}
    }

    upAge(){
    	for(var i=0;i<this.users.length;i++){
    		this.users[i].age = ++this.users[i].age; 
    	}
    }

    changeSpan(){
    	this.itemsOption[10].title = Math.random();
    	if(this.showSpan){
    		//this.showSpan = false;
    	} 
    	else {
    		//this.showSpan = true;
    	}
    }

    setData(type){
    	if(type === 'worker'){
    		this.users = this.workers;
    	}
    	else {
    		this.users = this.students;
    	}
    }


}