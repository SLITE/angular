import {Component} from "angular2/core";
import {Nav} from "../navigate/navigate";

 
@Component({
    selector: 'my-head',
    templateUrl: 'app/header/header.html',
    directives: [Nav]
})
 

export class Head {
    constructor() {
 
    }
}