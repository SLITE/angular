import {Component} from "angular2/core";
import {Router} from 'angular2/router';
 
@Component({
    selector: 'my-nav',
    templateUrl: 'app/navigate/navigate.html'
})
 

export class Nav {
    constructor(router: Router) {
 		this.router = router;
    }
    navTo(path){
		this.router.navigate([path]);
    }
}