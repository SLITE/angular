import {Component, View} from "angular2/core";
import {Head} from "../header/head";
 
@Component({
    selector: 'page2'
})
 
@View({
    templateUrl: 'app/page2/page2.html',
    directives: [Head]
})
 
export class Page2 {
    constructor() {
 
    }
}