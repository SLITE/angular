import {Http} from 'angular2/http';
import {Injectable} from "angular2/core";
import 'rxjs/Rx';

@Injectable()
export class DataProvider {
	constructor(private http: Http) { }
	getUsrs(url){
	return this.http.get(url);
		/*this.http.get(url)
	    .map(res => res.text())
	    .subscribe(
	      data => this.users = data,
	      err => console.log(err),
	      () => console.log('Data load')
	    );*/
	}
}